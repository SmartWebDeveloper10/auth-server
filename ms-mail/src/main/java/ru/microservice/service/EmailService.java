package ru.microservice.service;

import ru.microservice.entity.dto.UserDto;

public interface EmailService {

    void sendSimpleMessage(UserDto input);
}
