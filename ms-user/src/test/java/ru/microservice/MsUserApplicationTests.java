package ru.microservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest(classes = MsUserApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MsUserApplicationTests {

	@Test
	public void contextLoads() {
	}

}
