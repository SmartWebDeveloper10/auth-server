package ru.microservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest(classes = MsDiscoveryApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MsDiscoveryApplicationTests {

	@Test
	public void contextLoads() {
	}

}
