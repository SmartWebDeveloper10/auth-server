#!/bin/bash

# To get a JWT token for the admin, first run this: ./curlUser.sh in the terminal.
# The system will return a JWT token of the user with ROLE_USER authorities.
# In case of Bad Interpreter error, run:  sed -i -e 's/\r$//' ./curlUser.sh
export JAVA_HOME=/usr/lib/jvm/java-1.8.0-amazon-corretto

json=$( curl trusted-app:secret@localhost:9999/oauth/token -d grant_type=password -d username=user -d password=password )
token=$(echo $json | sed "s/{.*\"access_token\":\"\([^\"]*\).*}/\1/g")
echo $token
