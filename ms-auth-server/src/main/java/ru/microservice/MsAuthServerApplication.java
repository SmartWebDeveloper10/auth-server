package ru.microservice;

import ru.microservice.entity.Account;
import ru.microservice.entity.Role;
import ru.microservice.service.AccountService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.LinkedList;
import java.util.List;

@EnableEurekaClient
@EnableDiscoveryClient
@SpringBootApplication
public class MsAuthServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsAuthServerApplication.class, args);
    }


    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    CommandLineRunner init(
            AccountService accountService
    ) {

        List<Account> accountsList = new LinkedList<>();

        Account user = new Account();
        user.setUsername("user");
        user.setPassword("password");
        user.setTwoFa(false);

        accountsList.add(user);

        Account admin = new Account();
        admin.setUsername("admin");
        admin.setPassword("password");
        admin.grantAuthority(Role.ROLE_ADMIN);
        admin.setTwoFa(true);
        admin.setSecret("JBSWY3DPEHPK3PXP");

        accountsList.add(admin);

        return (evt) -> accountsList.forEach(
                acct -> {
                    accountService.registerUser(acct);
                }
        );
    }
}
