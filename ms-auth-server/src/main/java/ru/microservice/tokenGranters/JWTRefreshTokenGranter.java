package ru.microservice.tokenGranters;

import ru.microservice.config.AuthorizationConfig;
import ru.microservice.service.TfaService;
import org.springframework.security.authentication.AuthenticationManager;

import org.springframework.security.oauth2.common.OAuth2AccessToken;

import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;

import org.springframework.security.oauth2.provider.OAuth2Authentication;

import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.token.*;

import java.util.Map;

public class JWTRefreshTokenGranter extends AbstractTokenGranter {


    private static final String GRANT_TYPE = "refresh_token";

    private final TokenStore tokenStore;
    private final DefaultTokenServices jwtTokenService;
    private final AuthorizationConfig.RefreshTokenConverter jwtConverter;


    public JWTRefreshTokenGranter(AuthorizationServerEndpointsConfigurer endpointsConfigurer, AuthenticationManager authenticationManager, TfaService tfaService
            , DefaultTokenServices jwtTokenService,
                                  AuthorizationConfig.RefreshTokenConverter converter
    ) {
        super(jwtTokenService, endpointsConfigurer.getClientDetailsService(), endpointsConfigurer.getOAuth2RequestFactory(), GRANT_TYPE);

        this.tokenStore = endpointsConfigurer.getTokenStore();
        this.jwtTokenService = jwtTokenService;
        this.jwtConverter = converter;
    }

    public OAuth2AccessToken grant(String grantType, TokenRequest tokenRequest){
        if(!grantType.equals(GRANT_TYPE)){
            return null;
        }

        String refrToken = tokenRequest.getRequestParameters().get("refresh_token");
        //decode(refrtoken) verifies the token as well
        Map<String,Object> map = this.jwtConverter.decode(refrToken);

        OAuth2Authentication auth = this.jwtConverter.extractAuthentication(map);

        OAuth2AccessToken result = this.jwtTokenService.createAccessToken(auth);

        return result;


    }
}
