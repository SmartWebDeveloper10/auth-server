package ru.microservice.service;


import ru.microservice.entity.Account;

import ru.microservice.entity.dto.UserDto;

import org.springframework.stereotype.Service;


@Service
public interface AccountService  {



    Account registerUser(Account account);

    Account registerUser(UserDto userDto);

    boolean doesUserExist(String name);



}
