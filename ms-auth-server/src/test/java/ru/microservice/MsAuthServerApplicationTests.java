package ru.microservice;

import ru.microservice.entity.Account;
import ru.microservice.entity.Role;
import ru.microservice.service.AccountService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;

import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@SpringBootTest(classes = MsAuthServerApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MsAuthServerApplicationTests {

	private  MockMvc mockMvc;

	private Account user;

	private  Account admin;


	@Autowired
	private  WebApplicationContext context;

	@Autowired
	private AccountService accountService;

	@BeforeEach
	public void setup() {
		mockMvc = webAppContextSetup(context)
				.apply(SecurityMockMvcConfigurers.springSecurity()).build();

		user = getUser();
		admin = getAdmin();

		List<Account> people = new ArrayList<>();
		people.add(user); people.add(admin);

		setUsers(people);

	}

	@Test
	public void contextLoads() {
		assertTrue(true);

	}

	@Test
	public void jwtTokenIsReturnedForNoTfaUser() throws Exception {

		MvcResult result = mockMvc.perform(post("/oauth/token")
				.header("Authorization","Basic dHJ1c3RlZC1hcHA6c2VjcmV0")
				.param("username","user")
				.param("password","password")
				.param("grant_type","password")
				.contentType(MediaType.MULTIPART_FORM_DATA_VALUE)

				)
				.andExpect(status().isOk())
				.andReturn();

		String resp = result.getResponse().getContentAsString();
		System.out.println("the jwt result is "+resp);

		String tokenBody = getTokenString("access_token", resp);
		String user = getUserName(tokenBody);
		String auth = getAuthorities(tokenBody);

		assertNotEquals("", result.getResponse().getHeader("Authorization"));
		assertEquals(user, "user");
		assertTrue(auth.contains("ROLE_USER"));
		assertTrue(auth.length()<13);//checks there are no other ROLE_SMTH in this string

	}

	@Test
	public void jwtTokenIsReturnedForATfaUser() throws Exception {

		MvcResult result = mockMvc.perform(post("/oauth/token")
				.header("Authorization","Basic dHJ1c3RlZC1hcHA6c2VjcmV0")
				.param("username","admin")
				.param("password","password")
				.param("grant_type","password")
				.contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
		)
				.andExpect(status().isOk())
				.andReturn();

		String resp = result.getResponse().getContentAsString();
		System.out.println("the jwt result is "+resp);

		String token = getTokenString("tfa_token",resp);

		result = mockMvc.perform(post("/oauth/token")
				.header("Authorization","Basic dHJ1c3RlZC1hcHA6c2VjcmV0")
				.param("tfa_code","123456")
				.param("tfa_token",token)
				.param("grant_type","tfa")
				.contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
		)
				.andExpect(status().isOk())
				.andReturn();

		resp = result.getResponse().getContentAsString();

		String tokenBody = getTokenString("access_token",resp);
		String user = getUserName(tokenBody);
		String auth = getAuthorities(tokenBody);


		assertEquals(user, "admin");
		assertTrue(auth.contains("ROLE_USER"));
		assertTrue(auth.contains("ROLE_ADMIN"));

	}

	@Test
	public void jwtTokenIsObtainedOutOfRefreshToken() throws Exception{

		MvcResult result = mockMvc.perform(post("/oauth/token")
				.header("Authorization","Basic dHJ1c3RlZC1hcHA6c2VjcmV0")
				.param("username","user")
				.param("password","password")
				.param("grant_type","password")
				.contentType(MediaType.MULTIPART_FORM_DATA_VALUE)

		)
				.andExpect(status().isOk())
				.andReturn();

		String resp = result.getResponse().getContentAsString();
		System.out.println("the jwt result is "+resp);

		String token = getTokenString("refresh_token",resp);

		result = mockMvc.perform(post("/oauth/token")
				.header("Authorization","Basic dHJ1c3RlZC1hcHA6c2VjcmV0")
				.param("refresh_token",token)
				.param("grant_type","refresh_token")
				.contentType(MediaType.MULTIPART_FORM_DATA_VALUE)

		)
				.andExpect(status().isOk())
				.andReturn();
		resp = result.getResponse().getContentAsString();
		System.out.println(resp);


		token = getTokenString("access_token",resp);
		String user = getUserName(token);
		String auth = getAuthorities(token);

		assertNotEquals("", result.getResponse().getHeader("Authorization"));
		assertEquals(user, "user");
		assertTrue(auth.contains("ROLE_USER"));
		assertTrue(auth.length()<13);//checks there are no other ROLE_SMTH in this string
	}

	public static String getTokenString(String tokenType, String input){
		Pattern p = null;
		String token;
//Match the pattern of //access_token":" + 1 or more symbols other then quotes(")
		if(tokenType.equals("access_token")) p = Pattern.compile("(access_token\":\")[^\"]+");
		else if(tokenType.equals("refresh_token")) p = Pattern.compile("(refresh_token\":\")[^\"]+");
		else if(tokenType.equals("tfa_token")) p = Pattern.compile("(access_token\":\")[^\"]+");

		Matcher m = p.matcher(input);
		if (m.find()) {
			token = m.group();
		}
		else return "";

		token = token.substring(token.indexOf(":") + 2);
		if(tokenType.equals("tfa_token")) return token;
		else if (tokenType.equals("refresh_token")) return token;
		else return new String(Base64.getDecoder().decode(token.split("\\.")[1]));



	}

	public static String getUserName(String input){
		Pattern p = Pattern.compile("(user_name\":\")[^\"]+");
		Matcher m = p.matcher(input);

		String user;
		if(m.find()){
			user = m.group();
			user = user.substring(user.indexOf(":")+2);
			return user;
		}
		else return "";
	}

	public static String getAuthorities(String input){
		Pattern p = Pattern.compile("(authorities\":\\[\")[^\\]]+\\]");
		Matcher m = p.matcher(input);

		String user;
		if(m.find()){
			user = m.group();
			user = user.substring(user.indexOf(":")+2);
			return user;
		}
		else return "";
	}



	public static Account getUser(){
		Account user = new Account();
		user.setUsername("user");
		user.setPassword("password");
		user.setTwoFa(false);
		user.setSecret("");
		List<Role> roles = new ArrayList<>();
		roles.add(Role.ROLE_USER);
		user.setRoles(roles);
		return user;
	}

	public static Account getAdmin(){
		Account user = new Account();
		user.setUsername("admin");
		user.setPassword("password");
		user.setTwoFa(true);
		user.setSecret("12345");
		List<Role> roles = new ArrayList<>();

		roles.add(Role.ROLE_ADMIN);
		user.setRoles(roles);
		return user;
	}

	public void setUsers(List<Account> users){
		users.forEach(u->{
			if(!accountService.doesUserExist(u.getUsername())){
				accountService.registerUser(u);
			}

		});
	}

}
