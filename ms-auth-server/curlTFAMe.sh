#!/bin/bash

export JAVA_HOME=/usr/lib/jvm/java-1.8.0-amazon-corretto

curl trusted-app:secret@localhost:9999/oauth/token -d grant_type=tfa -d tfa_token=$1 -d tfa_code=197214
